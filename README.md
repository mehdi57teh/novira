# Novira



## About The Project
*Hello my friends*
This project is written with [FastAPI](https://fastapi.tiangolo.com/) framework and Rest architecture and has high performance.
[beanie-odm](http://beanie-odm.dev/) and Python are used in this project.


## Getting started

#### Prerequisites
  - [Python](https://www.python.org/downloads/) is installed 
  - [Docker](https://www.docker.com/) is installed 


#### Run the project
1.set **JWT_SECRET** and settings in .env file

2.run docker compose

        docker-compose up -d --build


## Description
In this project, we implement Shopping-Cart, that contains bellow APIs:
- [ ] [AddToCart]() : Add a product to cart
- [ ] [RemoveFromCart]() : Remove product from cart
- [ ] [List]() : Show list of products in shopping-cart
- [ ] [CompletePurchase]() : Complete the purchase 


We use JWT technology to Authentication/Authorization in all services.

### Dockerfile 

We Multi-stage builds in Dockerfile to optimize Dockerfiles while keeping them easy to read and maintain.. Multistage builds feature in Dockerfiles enables you to create smaller container images with better caching and smaller security footprint.


## Roadmap

Releases in the future :

- Put Core module in separate repository as installable package like requirements.
- Use scopes in JWT for authorization
- Add token for API call between services
- Implement Black Box Testing (Behavioral Testing), Service Test, Integration Test
