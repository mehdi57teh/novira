import unittest

from beanie import PydanticObjectId

from app.Infrastructure.cart import Cart
from app.models.cart_model import CartItem


class TestDeleteItem(unittest.TestCase):
    def test_delete_item_from_cart(self):
        # Arrange
        new_item1: CartItem = CartItem(
            user_id=PydanticObjectId('634554ccd8a7d40007a59e44'),
            product_id=PydanticObjectId('633559f7ef28316c528c0f23'),
            product_quantity=14
        )
        await new_item1.insert()
        new_item2: CartItem = CartItem(
            user_id=PydanticObjectId('634554ccd8a7d40007a59e44'),
            product_id=PydanticObjectId('6345578cd592bb12291c503e'),
            product_quantity=35
        )
        await new_item2.insert()

        item_id: PydanticObjectId = new_item1.id

        # Act
        Cart.delete_cart(item_id=item_id)

        # Assert
        deleted_item = CartItem.get(item_id)
        self.assertIsNone(deleted_item)


if __name__ == '__main__':
    unittest.main()
