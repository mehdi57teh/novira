import unittest

from beanie import PydanticObjectId

from app.Infrastructure.cart import Cart
from app.models.cart_model import CartItem


class TestDeleteItem(unittest.TestCase):
    def test_delete_item_from_cart(self):
        # Arrange
        user_id = PydanticObjectId('634554ccd8a7d40007a59e44')

        new_item1: CartItem = CartItem(
            user_id=user_id,
            product_id=PydanticObjectId('633559f7ef28316c528c0f23'),
            product_quantity=14
        )
        await new_item1.insert()
        new_item2: CartItem = CartItem(
            user_id=user_id,
            product_id=PydanticObjectId('6345578cd592bb12291c503e'),
            product_quantity=35
        )
        await new_item2.insert()

        # Act
        Cart.delete_all_cart_items(user_id=user_id)

        # Assert
        deleted_items = await CartItem.find(CartItem.user_id == user_id).to_list()
        self.assertEqual(len(deleted_items), 0)


if __name__ == '__main__':
    unittest.main()
