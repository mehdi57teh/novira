import unittest

from beanie import PydanticObjectId

from app.Infrastructure.cart import Cart
from app.models.cart_model import CartItem


class TestGetCartItemList(unittest.TestCase):
    def test_get_existing_item(self):
        # Arrange
        new_item1: CartItem = CartItem(
            user_id=PydanticObjectId('634554ccd8a7d40007a59e44'),
            product_id=PydanticObjectId('633559f7ef28316c528c0f23'),
            product_quantity=14
        )
        await new_item1.insert()
        new_item2: CartItem = CartItem(
            user_id=PydanticObjectId('634554ccd8a7d40007a59e44'),
            product_id=PydanticObjectId('6345578cd592bb12291c503e'),
            product_quantity=35
        )
        await new_item2.insert()

        user_id: PydanticObjectId = PydanticObjectId('634554ccd8a7d40007a59e44')

        # Act
        item_list = Cart.carts(user_id=user_id)

        # Assert
        self.assertEqual(len(item_list), 2)
        total_quantity: int = sum([item.product_quantity for item in item_list])
        self.assertEqual(total_quantity, 49)


if __name__ == '__main__':
    unittest.main()
