import unittest

from beanie import PydanticObjectId

from app.Infrastructure.cart import Cart
from app.models.cart_model import CartItem


class TestAddItemToCart(unittest.TestCase):
    def test_add_new_item(self):
        # Arrange
        user_id: PydanticObjectId = PydanticObjectId('634554ccd8a7d40007a59e44')
        product_id: PydanticObjectId = PydanticObjectId('633559f7ef28316c528c0f23')
        product_quantity: int = 14

        # Act
        new_item = Cart.add_to_cart(
            user_id = user_id,
            product_id = product_id,
            product_quantity = product_quantity
        )

        # Assert
        self.assertIsNotNone(new_item.id)

    async def test_add_existing_item(self):
        # Arrange
        new_item: CartItem = CartItem(
            user_id=PydanticObjectId('634554ccd8a7d40007a59e44'),
            product_id=PydanticObjectId('633559f7ef28316c528c0f23'),
            product_quantity=14
        )
        await new_item.insert()

        user_id: PydanticObjectId = PydanticObjectId('634554ccd8a7d40007a59e44')
        product_id: PydanticObjectId = PydanticObjectId('633559f7ef28316c528c0f23')
        product_quantity: int = 27

        # Act
        exis_item = Cart.add_to_cart(
            user_id = user_id,
            product_id = product_id,
            product_quantity = product_quantity
        )

        # Assert
        self.assertEqual(new_item.id, exis_item.id)
        self.assertEqual(new_item.product_quantity, 27)


if __name__ == '__main__':
    unittest.main()
