from decouple import config
from motor.motor_asyncio import AsyncIOMotorClient
from beanie import init_beanie, Document
from app.core.helper.WalkOnModule import WalkOnModule

_MONGO_DB_HOST = config("MONGO_DB_HOST")
_MONGO_DB_INTERNAL_PORT = int(config("MONGO_DB_INTERNAL_PORT"))
_MONGO_DB_NAME = config("MONGO_DB_NAME")
_MONGO_MOVED_DB_NAME = config("MONGO_MOVED_DB_NAME")
_MONGO_DB_USER = config("MONGO_DB_USER")
_MONGO_DB_PASS = config("MONGO_DB_PASS")

_MONGO_CONNECTION_URL = f"mongodb://{_MONGO_DB_USER}:{_MONGO_DB_PASS}@{_MONGO_DB_HOST}:{_MONGO_DB_INTERNAL_PORT}"


class Mongo:
    __client__ = None
    __db__ = None

    @classmethod
    async def connect(cls):
        """
            This function connect to mongodb and make singleton instance.
        """

        if cls.__client__ is None:
            cls.__client__ = AsyncIOMotorClient(_MONGO_CONNECTION_URL)
            cls.__db__ = cls.__client__[_MONGO_DB_NAME]
        else:
            raise Exception("We can not creat mongo connection")

    @classmethod
    async def get_db(cls):
        if not cls.__db__:
            await cls.connect()
        return cls.__db__


async def beanie_init():
    # Init beanie with the Product document class
    mongo_db = await Mongo.get_db()

    document_models: list = WalkOnModule.detect_all(Document)
    await init_beanie(database=mongo_db, document_models=document_models)
