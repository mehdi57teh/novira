from beanie import PydanticObjectId
from fastapi.security import OAuth2PasswordBearer
from fastapi import HTTPException, Depends, status
from pydantic import BaseModel

from .helper.API import sync_request, RequestMethod
from app import settings


oauth2_scheme = OAuth2PasswordBearer(tokenUrl='token')


class User(BaseModel):
    id: PydanticObjectId
    is_active: bool


def get_current_user(token: str = Depends(oauth2_scheme)):
    try:
        get_user_url = f'{settings.USER_SERVICE_BASE_URL}/verify_token'
        data = {'token': token}
        response = await sync_request(method=RequestMethod.POST, url=get_user_url, data=data)
        assert response.status_code == 200
        user: User = User(**response.json())

    except:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Invalid email or password'
        )

    return user
