from enum import Enum
from typing import Optional, Any
import requests


class RequestMethod(str, Enum):
    POST = "POST"
    GET = "GET"
    PUT = "PUT"
    PATCH = "PATCH"
    DELETE = "DELETE"
    SEND = "SEND"
    HEAD = "HEAD"
    OPTIONS = "OPTIONS"


async def sync_request(method: RequestMethod = "POST", url: str = None, data: Optional[Any] = None):
    assert url is not None

    # TODO: Add authentication in header in feature
    response = requests.request(method, url, data=data)
    return response


