import importlib
import os


class WalkOnModule(object):
    @classmethod
    def detect_all(cls, base_class: type, class_paths: str | list[str] = os.path.join('app', 'ModelClasses'),
                   excludes: list[str] = [], includes: list[str] = [], only_leaf: bool = True):
        file_list = []
        if isinstance(class_paths, str):
            class_paths = [class_paths]
        for class_path in class_paths:
            if os.path.isfile(class_path):
                file_list.append(class_path)
            elif os.path.isdir(class_path):
                for path, subdirs, files in os.walk(class_path):
                    for name in files:
                        if any([c in path or c in name for c in excludes]):
                            continue
                        if name.endswith('.py') and not name.startswith('_'):
                            file_list.append(os.path.join(path, name))
            else:
                raise Exception(f'The "classes_path" parameter is not file or folder. \nclasses_path={class_path}')

        for file in file_list:
            importlib.import_module(file.replace(os.sep, '.')[:-3])
        return cls.get_all_subclasses(base_class, excludes, includes, only_leaf)

    @classmethod
    def get_all_subclasses(cls, base_class: type,
                           excludes: list[str] = [], includes: list[str] = [], only_leaf: bool = True):
        """
        :param base_class is a target class
        :param excludes list of exclude string
        :param includes list of include string
        :param only_leaf if True return only class that has no subclass
        """
        all_classes = []
        for subclass in base_class.__subclasses__():
            if any([c in subclass.__module__ or c in subclass.__class__.__name__ for c in excludes]):
                continue
            if includes and any([c not in subclass.__module__ and c not in subclass.__class__.__name__
                                 for c in includes]):
                continue
            all_subclasses = cls.get_all_subclasses(subclass, excludes, includes, only_leaf)
            if not only_leaf or not all_subclasses:
                all_classes.append(subclass)
            all_classes.extend(all_subclasses)
        return all_classes
