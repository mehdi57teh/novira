from fastapi import FastAPI
from app.routes.cart_api import router
from app.core.database.beanie_db import beanie_init

app = FastAPI(openapi_url="/api/v1/cart/openapi.json", docs_url="/api/v1/cart/docs")


@app.on_event("startup")
async def startup():
    await beanie_init()


app.include_router(router, prefix='/api/v1/cart', tags=['cart'])
