from beanie import Document, PydanticObjectId


class CartItem(Document):
    user_id: PydanticObjectId
    product_id: PydanticObjectId
    product_quantity: int
