from decimal import Decimal

from beanie import PydanticObjectId
from pydantic import BaseModel, Field


class ProductItem(BaseModel):
    id: PydanticObjectId
    title: str = Field(..., max_length=50)
    description: str = Field(...)
    image: str = Field(..., max_length=100)
    price: Decimal = Field(..., max_digits=10, decimal_places=2)
