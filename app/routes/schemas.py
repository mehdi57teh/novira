from decimal import Decimal
from typing import List

from beanie import PydanticObjectId
from pydantic import BaseModel

from app.models.cart_model import CartItem


class AddToCart(BaseModel):
    product_id: PydanticObjectId
    quantity: int


class Items(CartItem):
    product_image: str
    product_price: Decimal


class Carts(BaseModel):
    total_price: Decimal
    items: List[Items] = []
