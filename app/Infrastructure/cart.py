from typing import List
from beanie import PydanticObjectId
from app.models.cart_model import CartItem


class Cart:
    @classmethod
    def add_to_cart(cls, **kwargs) -> CartItem:
        """
        add new item if not exist, and edit quantity if exist
        :param kwargs: contain user_id, product_id, product_quantity
        :return: cart item
        """
        user_id: PydanticObjectId = PydanticObjectId(kwargs['user_id'])
        product_id: PydanticObjectId = PydanticObjectId(kwargs['product_id'])
        product_quantity = int(kwargs['product_quantity'])
        # check if cart already exists
        user_carts: CartItem = await CartItem.find_one(CartItem.user_id == user_id, CartItem.product_id == product_id)
        if user_carts and user_carts.product_quantity != product_quantity:
            user_carts.product_quantity = product_quantity
            await user_carts.save()
            return user_carts

        new_item: CartItem = CartItem(
            user_id=user_id,
            product_id=product_id,
            product_quantity=product_quantity
        )
        await new_item.insert()
        return new_item

    @classmethod
    def carts(cls, user_id: PydanticObjectId) -> List[CartItem]:
        """
        List of all items in user cart
        :param user_id:
        :return:
        """
        cart_items = await CartItem.find(CartItem.user_id == user_id).to_list()
        return cart_items

    @classmethod
    def delete_cart(cls, item_id: PydanticObjectId):
        """
        delete item from cart
        :param item_id: id of document
        """
        item = await CartItem.get(item_id)
        item.delete()

    @classmethod
    def delete_all_cart_items(cls, user_id: PydanticObjectId):
        """
        delete item from user cart
        :param user_id: id of user
        """
        await CartItem.find(CartItem.user_id == user_id).delete()
