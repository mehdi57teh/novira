from typing import List, Union

from fastapi import HTTPException, status
from beanie import PydanticObjectId

from app import settings
from app.core.helper.API import sync_request, RequestMethod
from app.models.product_model import ProductItem


class Product:
    @classmethod
    def get_product(cls, product_id: Union[PydanticObjectId, List[PydanticObjectId]]) -> List[ProductItem]:
        """
        get product(s) from Product service's API
        :param product_id: id of product(s)
        :return: list of products
        """
        if not isinstance(product_id, List):
            product_id = [product_id]

        if len(product_id) < 1:
            raise HTTPException(
                status_code = status.HTTP_400_BAD_REQUEST,
                detail = 'Bad request'
            )

        try:
            get_user_url = f'{settings.PRODUCT_SERVICE_BASE_URL}/products'
            data = {'ids': product_id}
            response = await sync_request(method = RequestMethod.GET, url = get_user_url, data = data)
            assert response.status_code == 200
            product: List[ProductItem] = [ProductItem(**item) for item in response.json()]
        except:
            raise HTTPException(
                status_code = status.HTTP_404_NOT_FOUND,
                detail = 'Not Found'
            )

        return product
