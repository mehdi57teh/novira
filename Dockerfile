FROM python:3.10.5 as builder

WORKDIR /app

COPY requirements.txt /app/requirements.txt

RUN apt-get update \
    && apt-get install gcc -y \
    && apt-get clean

RUN python -m venv /opt/venv
# Make sure we use the virtualenv:
ENV PATH="/opt/venv/bin:$PATH"

RUN ["chmod", "+x", "/opt/venv/bin/activate"]
RUN source bin/activate

RUN python -m pip install --upgrade pip
RUN pip install -r /app/requirements.txt \
    && rm -rf /root/.cache/pip

#COPY /app /usr/app
COPY cart-service /app/

FROM python:3.10.5 as release
RUN apk --no-cache add ca-certificates
WORKDIR /app
COPY --from=builder cart-service /app
COPY --from=builder /opt/venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH"
RUN ["chmod", "+x", "/opt/venv/bin/activate"]
RUN source bin/activate

CMD ["/bin/bash"]
